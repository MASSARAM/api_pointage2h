import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeOrmConfig } from './config/typeorm-config';
import { EquipesModule } from './equipes/equipes.module';
import { DepartementsModule } from './departements/departements.module';
import { EmployesModule } from './employes/employes.module';
import { PointagesModule } from './pointages/pointages.module';

@Module({
  imports: [ TypeOrmModule.forRoot(typeOrmConfig), EquipesModule, DepartementsModule, EmployesModule, PointagesModule],
})
export class AppModule {}
