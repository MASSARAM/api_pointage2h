import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";
import { Horodatage } from './../../utils/horodatage';

@Entity('equipes')
export class Equipe extends Horodatage{
    @PrimaryGeneratedColumn()
    id_equipe:number
    
    @Column()
    nom_equipe:string
  
}