import { Injectable } from '@nestjs/common';
import { EquipeDto } from './equipe.dto';

@Injectable()
export class EquipesService {
  create(createEquipeDto: EquipeDto) {
    return 'This action adds a new equipe';
  }

  findAll() {
    return `This action returns all equipes`;
  }

  findOne(id: number) {
    return `This action returns a #${id} equipe`;
  }

  update(id: number, updateEquipeDto: EquipeDto) {
    return `This action updates a #${id} equipe`;
  }

  remove(id: number) {
    return `This action removes a #${id} equipe`;
  }
}
