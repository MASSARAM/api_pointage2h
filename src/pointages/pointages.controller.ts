import { Controller, Get, Post, Body, Put, Param, Delete } from '@nestjs/common';
import { PointagesService } from './pointages.service';
import { CreatePointageDto } from './dto/create-pointage.dto';
import { UpdatePointageDto } from './dto/update-pointage.dto';

@Controller('pointages')
export class PointagesController {
  constructor(private readonly pointagesService: PointagesService) {}

  @Post()
  create(@Body() createPointageDto: CreatePointageDto) {
    return this.pointagesService.create(createPointageDto);
  }

  @Get()
  findAll() {
    return this.pointagesService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.pointagesService.findOne(+id);
  }

  @Put(':id')
  update(@Param('id') id: string, @Body() updatePointageDto: UpdatePointageDto) {
    return this.pointagesService.update(+id, updatePointageDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.pointagesService.remove(+id);
  }
}
