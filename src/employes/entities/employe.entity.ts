import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";
import { Horodatage } from './../../utils/horodatage';

@Entity('employes')
export class Employe extends Horodatage{
@PrimaryGeneratedColumn()
id_employe

@Column()
nom_employe:string

@Column()
prenom_employe:string

@Column()
motpasse:string
}
