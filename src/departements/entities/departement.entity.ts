import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";
import { Horodatage } from './../../utils/horodatage';

@Entity('departements')
export class Departement extends Horodatage{
    @PrimaryGeneratedColumn()
    id_departement
    @Column()
    nom_departement:string

}
